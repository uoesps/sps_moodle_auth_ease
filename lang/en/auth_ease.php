<?php

$string['pluginname'] = 'EASE Authentication';
$string['auth_easedescription'] = 'Once signed into EASE will pick up username and login';
$string['EASEform'] = 'Authentication choice';
$string['auth_ease_server_settings'] = 'EASE server configuration';
$string['auth_ease_cosign_service'] = 'eg cosign-eucsCosign-' . $_SERVER['HTTP_HOST'];
$string['auth_ease_cosign_service_key'] = 'Cosign Service';
$string['auth_ease_cosign_logout_url'] = 'eg https://www.ease.ed.ac.uk/logout/logout.cgi';
$string['auth_ease_cosign_logout_url_key'] = 'Cosign Logout URL';

?>
