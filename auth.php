<?php

  /**
   * @author Keith Brown
   *
   * Based On Authentication Plugin: CAS Authentication
   *
   * @author Martin Dougiamas
   * @author Jerome GUTIERREZ
   * @author I�aki Arenaza
   * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
   */

if (!defined('MOODLE_INTERNAL')) {
  die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->dirroot.'/auth/ldap/auth.php');

class auth_plugin_ease extends auth_plugin_ldap {

  /**
   * Constructor.
   */
  function auth_plugin_ease() {
    $this->authtype = 'ease';
    $this->roleauth = 'auth_ease';
    $this->errorlogtag = '[AUTH EASE] ';
    $this->init_plugin($this->authtype);
  }

  function prevent_local_passwords() {
    return true;
  }

  /**
   * Checls EASE realm and returns username.
   *
   * @return bool Authentication success or failure.
   */
  function user_login($username,$password) {

	  return true;
  }

  /**
   * Returns true if this authentication plugin is 'internal'.
   *
   * @return bool
   */
  function is_internal() {
    return false;
  }

  /**
   * Returns true if this authentication plugin can change the user's
   * password.
   *
   * @return bool
   */
  function can_change_password() {
    return false;
  }


function loginpage_hook() {

  global $frm;
  global $CFG;
  global $SESSION, $OUTPUT, $PAGE;

  if(isset($_SERVER['REMOTE_USER'])) {
    $frm->username = $_SERVER['REMOTE_USER'] ;
    $frm->password = 'passwdEASE';
    return;
    }

}

function prelogout_hook() {
  global $redirect;

setcookie($_SERVER['COSIGN_SERVICE'], "null", time()-1, '/', "", 1 );

$redirect = "https://www.ease.ed.ac.uk/logout/logout.cgi";

}

    /**
     * Prints a form for configuring this authentication plugin.
     *
     * This function is called from admin/auth.php, and outputs a full page with
     * a form for configuring this plugin.
     *
     * @param array $page An object containing all the data for this page.
     */
    function config_form($config, $err, $user_fields) {
        global $CFG, $OUTPUT;

        if (!function_exists('ldap_connect')) { // Is php-ldap really there?
            echo $OUTPUT->notification(get_string('auth_ldap_noextension', 'auth_ldap'));

            // Don't return here, like we do in auth/ldap. We cas use CAS without LDAP.
            // So just warn the user (done above) and define the LDAP constants we use
            // in config.html, to silence the warnings.
            if (!defined('LDAP_DEREF_NEVER')) {
                define ('LDAP_DEREF_NEVER', 0);
            }
            if (!defined('LDAP_DEREF_ALWAYS')) {
            define ('LDAP_DEREF_ALWAYS', 3);
            }
        }

        include($CFG->dirroot.'/auth/ease/config.html');
    }

    function sync_roles($user) {
		return;
    }

    /**
     * Processes and stores configuration data for this authentication plugin.
     */
    function process_config($config) {

        // EASE settings
        if (!isset($config->cosign_service)) {
            $config->cosign_service = '';
        }

        // LDAP settings
        if (!isset($config->host_url)) {
            $config->host_url = '';
        }
        if (empty($config->ldapencoding)) {
            $config->ldapencoding = 'utf-8';
        }
        if (!isset($config->contexts)) {
            $config->contexts = '';
        }
        if (!isset($config->user_type)) {
            $config->user_type = 'default';
        }
        if (!isset($config->user_attribute)) {
            $config->user_attribute = '';
        }
        if (!isset($config->search_sub)) {
            $config->search_sub = '';
        }
        if (!isset($config->opt_deref)) {
            $config->opt_deref = LDAP_DEREF_NEVER;
        }
        if (!isset($config->bind_dn)) {
            $config->bind_dn = '';
        }
        if (!isset($config->bind_pw)) {
            $config->bind_pw = '';
        }
        if (!isset($config->ldap_version)) {
            $config->ldap_version = '3';
        }
        if (!isset($config->objectclass)) {
            $config->objectclass = '';
        }
        if (!isset($config->memberattribute)) {
            $config->memberattribute = '';
        }

        if (!isset($config->memberattribute_isdn)) {
            $config->memberattribute_isdn = '';
        }
        if (!isset($config->attrcreators)) {
            $config->attrcreators = '';
        }
        if (!isset($config->groupecreators)) {
            $config->groupecreators = '';
        }
        if (!isset($config->removeuser)) {
            $config->removeuser = AUTH_REMOVEUSER_KEEP;
        }

        // save CAS settings
        set_config('cosign_service', trim($config->cosign_service), $this->pluginconfig);

        // save LDAP settings
        set_config('host_url', trim($config->host_url), $this->pluginconfig);
        set_config('ldapencoding', trim($config->ldapencoding), $this->pluginconfig);
        set_config('contexts', trim($config->contexts), $this->pluginconfig);
        set_config('user_type', moodle_strtolower(trim($config->user_type)), $this->pluginconfig);
        set_config('user_attribute', moodle_strtolower(trim($config->user_attribute)), $this->pluginconfig);
        set_config('search_sub', $config->search_sub, $this->pluginconfig);
        set_config('opt_deref', $config->opt_deref, $this->pluginconfig);
        set_config('bind_dn', trim($config->bind_dn), $this->pluginconfig);
        set_config('bind_pw', $config->bind_pw, $this->pluginconfig);
        set_config('ldap_version', $config->ldap_version, $this->pluginconfig);
        set_config('objectclass', trim($config->objectclass), $this->pluginconfig);
        set_config('memberattribute', moodle_strtolower(trim($config->memberattribute)), $this->pluginconfig);
        set_config('memberattribute_isdn', $config->memberattribute_isdn, $this->pluginconfig);
        set_config('attrcreators', trim($config->attrcreators), $this->pluginconfig);
        set_config('groupecreators', trim($config->groupecreators), $this->pluginconfig);
        set_config('removeuser', $config->removeuser, $this->pluginconfig);

        return true;
    }

    function user_authenticated_hook(&$user, $username, $password) {
#        system("/var/www/mgc/courses/auth/ease/cli/runsync");
    }


}

?>
